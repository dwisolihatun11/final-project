<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Peran extends Model
{
    protected $table = "perans";
    protected $fillable = ['nama','cast_id','film_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function cast(){
        return $this->belongsTo('App\Cast');
    }
    public function film(){
        return $this->belongsTo('App\Film');
    }
}
