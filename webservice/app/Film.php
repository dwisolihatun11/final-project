<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Film extends Model
{
    protected $table = "films";
    protected $fillable = ['judul','ringkasan','tahun','poster','genre_id', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function peran(){
        return $this->hasMany('App\Peran');
    }

    public function genre(){
        return $this->belongsTo('App\Genre');
    }
    public function ulasan(){
        return $this->hasMany('App\Ulasan');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
