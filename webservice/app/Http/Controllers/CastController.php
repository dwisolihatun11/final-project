<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CastController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }
    
    public function index()
    {
        $casts = Cast::latest()->get();
        // dd($casts);
        return response()->json([
            'success' => true,
            'message' => 'Daftar Cast dari Film yang dimilihi ole Bioskop Kita',
            'data' => $casts
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $casts = Cast::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        if ($casts) {
            return response()->json([
                'success' => true,
                'message' => 'Cast Berhasil Dibuat',
                'data' => $casts
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Cast Gagal Dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $casts = Cast::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => "Detail Cast",
            'data' => $casts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cast $cast)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $cast = Cast::findOrFail($cast->id);
        if ($cast) {
            $cast->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Cast Berhasil Diubah',
                'data' => $cast
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Cast Tidak Ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $casts = Cast::findOrFail($id);
        if ($casts) {
            $casts->delete();
            return response()->json([
                'success' => true,
                'message' => 'Cast Berhasil Dihapus'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Cast Tidak Ditemukan'
        ], 404);
    }
}
