<?php

namespace App\Http\Controllers;

use App\Peran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PeranController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }
    
    public function index()
    {
        $peran = Peran::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Daftar Peran dari Film yang dimilihi ole Bioskop Kita',
            'data' => $peran
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $peran = Peran::create([
            'nama' => $request->nama,
            'cast_id' => $request->cast_id,
            'film_id' => $request->film_id,
        ]);

        if ($peran) {
            return response()->json([
                'success' => true,
                'message' => 'Peran Berhasil Dibuat',
                'data' => $peran
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Peran Gagal Dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => "Detail Cast",
            'data' => $peran
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peran $peran)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $peran = Peran::findOrFail($peran->id);
        if ($peran) {
            $peran->update([
                'nama' => $request->nama,
                'cast_id' => $request->cast_id,
                'film_id' => $request->film_id
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Peran Berhasil Diubah',
                'data' => $peran
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Peran Tidak Ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::findOrFail($id);
        if ($peran) {
            $peran->delete();
            return response()->json([
                'success' => true,
                'message' => 'Peran Berhasil Dihapus'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Peran Tidak Ditemukan'
        ], 404);
    }
}
