<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('Hai Ini Halaman Verifikasi');
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        
        $otp_code = OtpCode::where('otp',$request->otp)->first();

        if(!$otp_code)
        {
            return response()->json([
                'success' => false,
                'message' => 'Otp Code Tidak ditemukan'
            ]);
        }
        $now = Carbon::now();

        if ($now > $otp_code->valid_until ){
            return response()->json([
                'success' => false,
                'message' => 'Code sudah tidak berlaku lagi'
            ],400);
        }
        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);
        $otp_code->delete();
        return response()->json([
            'success' => true,
            'message' => 'User berhasil diverifikasi',
            'data' => $user
        ], 200);
    }
}
