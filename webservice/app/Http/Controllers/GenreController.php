<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api')
                    ->only(['store','update','destroy']);
                    // ->except(['login']);
    }

    public function index()
    {
        $genre = Genre::latest()->get();
        // dd($genre);
        return response()->json([
            'success' => true,
            'message' => 'Daftar Genre Film yang Tersedia di Bioskop Kita',
            'data' => $genre
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $genre = Genre::create([
            'nama' => $request->nama
        ]);

        if ($genre) {
            return response()->json([
                'success' => true,
                'message' => 'Genre Film Berhasil Dibuat',
                'data' => $genre
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Genre Gagal Dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Genre Film',
            'data' => $genre
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $genre = Genre::findOrFail($genre->id);

        if ($genre) {
            $genre->update([
                'nama' => $request->nama
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Genre Berhasil Diubah',
                'data' => $genre
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Genre Tidak Ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::findOrFail($id);
        if ($genre) {
            $genre->delete();
            return response()->json([
                'success' => true,
                'message' => 'Genre Berhasil Dihapus'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Genre Tidak Ditemukan'
        ], 404);
    }
}
