<?php

namespace App\Http\Controllers;

use App\Ulasan;
use Illuminate\Http\Request;
use App\Events\UlasanStoreEvent;
use Illuminate\Support\Facades\Validator;

class UlasanController extends Controller
{
    public function index()
    {
        $ulasan = Ulasan::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Daftar Ulasan dari Film yang dimilihi ole Bioskop Kita',
            'data' => $ulasan
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'isi' => 'required',
            'point' => 'required',
            'film_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();
        $ulasan = Ulasan::create([
            'isi' => $request->isi,
            'point' => $request->point,
            'film_id' => $request->film_id,
            'user_id' => $user->id
        ]);

        // Memanggil Event
        event(new UlasanStoreEvent($ulasan));

        if ($ulasan) {
            return response()->json([
                'success' => true,
                'message' => 'Ulasan Berhasil Dibuat',
                'data' => $ulasan
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Ulasan Gagal Dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ulasan = Ulasan::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => "Detail Cast",
            'data' => $ulasan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ulasan $ulasan)
    {
        $validator = Validator::make($request->all(), [
            'isi' => 'required',
            'point' => 'required',
            'film_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $ulasan = Ulasan::findOrFail($ulasan->id);
        if ($ulasan) {
            $ulasan->update([
                'isi' => $request->isi,
                'point' => $request->point,
                'film_id' => $request->film_id,
                'user_id' => auth()->user()->id
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Ulasan Berhasil Diubah',
                'data' => $ulasan
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Ulasan Tidak Ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ulasan = Ulasan::findOrFail($id);
        if ($ulasan) {
            $ulasan->delete();
            return response()->json([
                'success' => true,
                'message' => 'Ulasan Berhasil Dihapus'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Peran Tidak Ditemukan'
        ], 404);
    }
}
