<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','destroy']);
    }

    public function index()
    {
        $films = Film::latest()->get();
        // dd($films);

        return response()->json([
            'success' => true,
            'message' => 'Daftar Film Yang Sedang Tayang',
            'data' => $films
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);
        
        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }

        $films = Film::create([
            'judul' => $request->judul,
            'ringkasan' => $request->ringkasan,
            'tahun' => $request->tahun,
            'poster' => $request->poster,
            'genre_id' => $request->genre_id,
            'user_id' => auth()->user()->id
        ]);

        if ($films) {
            return response()->json([
                'success' => true,
                'message' => 'Film Berhasil Dibuat',
                'data' => $films
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Film Gagal Dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $films = Film::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Film',
            'data' => $films
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Film $film)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $films = Film::findOrFail($film->id);
        // dd($films);

        if ($films) {
            $films->update([
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' => $request->poster,
                'genre_id' => $request->genre_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Film Berhasil Diubah',
                'data' => $films
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Film Tidak Ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $films = Film::findOrFail($id);
        if ($films) {
            $films->delete();
            return response()->json([
                'success' => true,
                'message' => 'Film Berhasil Dihapus'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Film Tidak Ditemukan'
        ], 404);
    }
}
