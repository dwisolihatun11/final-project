<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }
        $profile = Profile::create([
            'umur' => $request->umur,
            'bio' => $request->bio,
            'user_id' => auth()->user()->id
        ]);

        if ($profile) {
            return response()->json([
                'success' => true,
                'message' => 'Profile Berhasil Dibuat',
                'data' => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Profile Gagal Dibuat'
        ], 409);
    }
    public function update(Request $request, Profile $profile)
    {
        $validator = Validator::make($request->all(), [
            'umur' => 'required',
            'bio' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::findOrFail($profile->id);

        if ($profile) {
            $profile->update([
                'umur' => $request->umur,
                'bio' => $request->bio
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Profile Berhasil Diubah',
                'data' => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Profile Tidak Ditemukan'
        ], 404);
    }

}
