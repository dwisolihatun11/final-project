<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Ulasan extends Model
{
    protected $table = "ulasans";
    protected $fillable = ['isi','point','film_id','user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
    public function film(){
        return $this->belongsTo('App\Film');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
