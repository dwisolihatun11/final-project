<?php

namespace App\Events;

use App\Ulasan;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UlasanStoreEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $ulasan;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Ulasan $ulasan)
    {
        $this->ulasan = $ulasan;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }
}
