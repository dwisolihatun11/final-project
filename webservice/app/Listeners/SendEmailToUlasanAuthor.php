<?php

namespace App\Listeners;

use App\Mail\UlasanAuthorMail;
use App\Events\UlasanStoreEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToUlasanAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UlasanStoreEvent  $event
     * @return void
     */
    public function handle(UlasanStoreEvent $event)
    {
        Mail::to($event->ulasan->user->email)->send(new UlasanAuthorMail($event->ulasan));
    }
}
