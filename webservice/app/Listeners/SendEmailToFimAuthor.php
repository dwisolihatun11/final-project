<?php

namespace App\Listeners;

use App\Mail\FilmAuthorMail;
use App\Events\UlasanStoreEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToFimAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UlasanStoreEvent  $event
     * @return void
     */
    public function handle(UlasanStoreEvent $event)
    {
        Mail::to($event->ulasan->film->user->email)->send(new FilmAuthorMail($event->ulasan));
    }
}
