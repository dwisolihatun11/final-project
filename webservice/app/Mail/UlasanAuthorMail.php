<?php

namespace App\Mail;

use App\Ulasan;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UlasanAuthorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ulasan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ulasan $ulasan)
    {
        $this->ulasan = $ulasan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.ulasan.ulasan_author_mails')
                    ->subject('Final Project Full Stack Web Developer');
    }
}
